package it.cherrychain.sck.port

import io.restassured.RestAssured.given
import io.vertx.core.Vertx.vertx
import io.vertx.ext.web.Router.router
import it.cherrychain.sck.checkout.db.JsonDb
import it.cherrychain.sck.checkout.port.GetCheckouts
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

internal class GetCheckoutsTest {
  private val vertx = vertx()
  private val router = router(vertx)
  private val memdb = mock(JsonDb::class.java)

  @BeforeEach
  internal fun setUp() {
    GetCheckouts(router, memdb)()
    with(vertx) {
      createHttpServer()
        .requestHandler(router)
        .listen(1234)
    }
  }

  @Test
  fun `should get something`() {
    given()
      .port(1234)
  }
}