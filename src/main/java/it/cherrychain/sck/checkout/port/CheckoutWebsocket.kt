package it.cherrychain.sck.checkout.port

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.SockJSHandler

fun websocket(vertx: Vertx, router: Router) = with(router) {
  val sockjs = SockJSHandler.create(vertx).socketHandler { socket ->
    vertx.eventBus().consumer<JsonObject>("checkout.events") {
      it.body().let {

      }
    }
  }

  route("/websocket/*").handler {  }
}