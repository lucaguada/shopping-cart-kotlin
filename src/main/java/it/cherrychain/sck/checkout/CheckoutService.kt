package it.cherrychain.sck.checkout

import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.core.Vertx.vertx
import io.vertx.ext.web.Router.router
import it.cherrychain.sck.checkout.port.checkoutApi
import it.cherrychain.sck.checkout.port.webroot

class CheckoutService : AbstractVerticle() {
  override fun start() {
    router(vertx)
      .also { webroot(it) }
      .also { checkoutApi(vertx, it) }
      .also {
        vertx.createHttpServer()
          .requestHandler(it)
          .listen(8080)
      }
  }
}

fun main(args: Array<String>) {
  vertx().deployVerticle(CheckoutService())
}