package it.cherrychain.sck.checkout.domain


interface Command

typealias CheckoutCommand = Command

data class CreateCheckout(
  val id: String,
  val item: String,
  val price: Double
) : CheckoutCommand

data class AcknowledgeCheckout(
  val id: String,
  val orderId: String
) : CheckoutCommand

data class ProcessCheckout(
  val id: String,
  val orderId: String,
  val paymentId: String
) : CheckoutCommand
