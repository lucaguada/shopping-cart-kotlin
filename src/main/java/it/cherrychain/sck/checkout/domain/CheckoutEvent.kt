package it.cherrychain.sck.checkout.domain

interface Event

typealias CheckoutEvent = Event

data class CheckoutCreated(
  val id: String,
  val item: String,
  val price: Double
) : CheckoutEvent

data class CheckoutAcknowledged(
  val id: String,
  val orderId: String
) : CheckoutEvent

data class CheckoutProcessed(
  val id: String,
  val orderId: String,
  val paymentId: String
) : CheckoutEvent