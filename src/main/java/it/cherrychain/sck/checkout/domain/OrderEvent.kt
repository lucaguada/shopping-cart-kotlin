package it.cherrychain.sck.checkout.domain

typealias OrderEvent = Event

data class OrderCreated(
  val id: String,
  val timestamp: Long,
  val checkoutId: String
) : OrderEvent

data class OrderAcknowledged(
  val id: String,
  val checkoutId: String,
  val timestamp: Long
) : OrderEvent