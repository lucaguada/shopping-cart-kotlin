package it.cherrychain.sck.checkout.logic

import io.vertx.core.json.JsonObject
import it.cherrychain.sck.checkout.domain.*
import org.slf4j.LoggerFactory
import java.util.function.Function

private val log = LoggerFactory.getLogger(AsEventImpl::class.java)

interface AsEvent : Function<JsonObject, Event?>

class AsEventImpl : AsEvent {
  override
  fun apply(json: JsonObject): Event? = json.let {
    val type = it.getString("type")
    val payload = it.getJsonObject("payload")

    when (type) {
      "checkout-created" -> payload.mapTo(CheckoutCreated::class.java)
      "checkout-acknowledged" -> payload.mapTo(CheckoutAcknowledged::class.java)
      "checkout-processed" -> payload.mapTo(CheckoutProcessed::class.java)
      "order-created" -> payload.mapTo(OrderCreated::class.java)
      "order-acknowledged" -> payload.mapTo(OrderAcknowledged::class.java)
      else -> log.warn("Event {} not handled", type).let { null }
    }
  }
}